import express from 'express';

const app = express();

app.get('/', (req, res) => {
    return res.json({
        message: 'Hola mundo de typescript'
    });
});

app.listen(3333, () => console.log('Server listo! √ en puerto: ', 3333));